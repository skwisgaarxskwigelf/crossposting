# Crossposting from Telegram to VKontakte UI

### How to:
* Create user and database and grand all priveleges on it (PostgreSQL) 
```
sudo -u postgres -i
psql
create database crossposting;
create user crossposting_user with encrypted password 'pass';
grant all privileges on database crossposting to crossposting_user;
```
* Do migrations: <br>
`flask db init` <br>
`flask db migrate` <br>
`flask db upgrade`
* Create dir *instance* in project root directory with *config.py* in it. File should content: <br>
**SECRET_KEY** <br>
**SQLALCHEMY_DATABASE_URI**
* Add a very simple nginx conf:
```
server {
    listen 80;
    server_name  ;

    location / {
        include uwsgi_params;
        uwsgi_pass unix:/var/www/crossposting/crossposting.sock;
    }
}
```
* Create *crossposting.service* in */etc/systemd/system* with following content:
```
[Unit]
Description=uWSGI instance to serve crossposting service
After=network.target

[Service]
User=sabi
Group=www-data
WorkingDirectory=/var/www/crossposting
Environment="PATH=/var/www/crossposting/venv/bin"
ExecStart=/var/www/crossposting/venv/bin/uwsgi --ini crossposting.ini

[Install]
WantedBy=multi-user.target
```
* `sudo systemctl enable crossposting.service` <br>
`sudo systemctl start crossposting.service` <br>
`sudo systemctl status crossposting`
* `export FLASK_ENV=development` <br>
`flask run --host=0.0.0.0`
